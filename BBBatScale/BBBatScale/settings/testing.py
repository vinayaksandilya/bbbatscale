import os

os.environ.setdefault("WEBHOOKS", "enabled")

from .development import *  # noqa: F401,F403,E402
