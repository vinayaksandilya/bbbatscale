# Generated by Django 3.2.4 on 2021-09-08 08:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("core", "0028_generalparameter_option_hide_statistics"),
    ]

    operations = [
        migrations.AddField(
            model_name="room",
            name="direct_join_secret",
            field=models.CharField(blank=True, max_length=255),
        ),
    ]
