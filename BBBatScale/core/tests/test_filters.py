from core.constants import SERVER_STATE_DISABLED, SERVER_STATE_UP
from core.filters import RoomFilter, SchedulingStrategyFilter, ServerFilter
from core.models import Room, RoomConfiguration, SchedulingStrategy, Server
from django.contrib.sites.models import Site
from django.http import QueryDict
from django.test import TestCase
from django.test.client import RequestFactory


class CoreFilterTestCase(TestCase):
    def setUp(self) -> None:
        self.testserver = Site.objects.create(name="testserver", domain="testserver")
        self.fbi = SchedulingStrategy.objects.create(name="FBI")
        self.hda = SchedulingStrategy.objects.create(name="HDA")
        self.hda.tenants.add(self.testserver)
        self.room_config1 = RoomConfiguration.objects.create(name="Config1")
        self.room_config2 = RoomConfiguration.objects.create(name="Config2")
        self.bbb1_server = Server.objects.create(
            dns="bbb1.example.org",
            datacenter="fbi",
            scheduling_strategy=self.fbi,
            state=SERVER_STATE_UP,
        )
        self.bbb2_server = Server.objects.create(
            dns="bbb2.other.example.org",
            datacenter="fbi1337",
            scheduling_strategy=self.fbi,
            state=SERVER_STATE_DISABLED,
        )
        self.bbb3_server = Server.objects.create(
            dns="bbb3.other.example.org", datacenter="hda", scheduling_strategy=self.hda, state=SERVER_STATE_UP
        )
        self.room1 = Room.objects.create(name="D14/00.01", scheduling_strategy=self.fbi, config=self.room_config1)
        self.room1.tenants.add(self.testserver)
        self.room2 = Room.objects.create(name="D14/00.02", scheduling_strategy=self.fbi, config=self.room_config1)
        self.room2.tenants.add(self.testserver)
        self.room3 = Room.objects.create(name="D15/00.03", scheduling_strategy=self.fbi, config=self.room_config2)
        self.room3.tenants.add(self.testserver)
        self.room4 = Room.objects.create(
            name="Mathlab", scheduling_strategy=self.hda, config=self.room_config2, server=self.bbb2_server
        )

    def test_room_filter(self):
        get_params = QueryDict("", mutable=True)
        get_params.update({"name": "/"})
        request = RequestFactory().get("/")
        f = RoomFilter(get_params, queryset=Room.objects.filter(tenants__in=[self.testserver]), request=request)
        self.assertQuerysetEqual(f.qs, Room.objects.filter(name__icontains="/"), transform=lambda x: x)
        get_params.pop("name")
        get_params.update({"scheduling_strategy": self.fbi.pk})
        f = RoomFilter(get_params, Room.objects.filter(tenants__in=[self.testserver]), request=request)
        self.assertQuerysetEqual(f.qs, Room.objects.filter(scheduling_strategy=self.fbi.pk), transform=lambda x: x)
        get_params.pop("scheduling_strategy")
        get_params.update({"server": self.bbb2_server.pk})
        f = RoomFilter(get_params, Room.objects.all(), request=request)
        self.assertQuerysetEqual(f.qs, Room.objects.filter(server=self.bbb2_server.pk), transform=lambda x: x)
        get_params.pop("server")
        get_params.update({"config": self.room_config2.pk})
        f = RoomFilter(get_params, Room.objects.all(), request=request)
        self.assertQuerysetEqual(f.qs, Room.objects.filter(config=self.room_config2.pk), transform=lambda x: x)

    def test_server_filter(self):
        get_params = QueryDict("", mutable=True)
        get_params.update({"dns": "other.example.org"})
        f = ServerFilter(get_params, Server.objects.all())
        self.assertQuerysetEqual(f.qs, Server.objects.filter(dns__icontains="other.example.org"), transform=lambda x: x)
        get_params.pop("dns")
        get_params.update({"scheduling_strategy": str(self.hda.pk)})
        get = RequestFactory().get("/")
        f = ServerFilter(get_params, Server.objects.all(), request=get)
        self.assertQuerysetEqual(f.qs, Server.objects.filter(scheduling_strategy=self.hda.pk), transform=lambda x: x)
        get_params.pop("scheduling_strategy")
        get_params.update({"dns": "fbi1337"})
        f = ServerFilter(get_params, Server.objects.all())
        self.assertQuerysetEqual(f.qs, Server.objects.filter(datacenter__icontains="fbi1337"), transform=lambda x: x)
        get_params.pop("dns")
        get_params.update({"state": SERVER_STATE_UP})
        f = ServerFilter(get_params, Server.objects.all())
        self.assertQuerysetEqual(f.qs, Server.objects.filter(state=SERVER_STATE_UP), transform=lambda x: x)

    def test_scheduling_strategy_filter(self):
        get_params = QueryDict("", mutable=True)
        get_params.update({"name": "HDA"})
        f = SchedulingStrategyFilter(get_params, SchedulingStrategy.objects.all())
        self.assertQuerysetEqual(f.qs, SchedulingStrategy.objects.filter(name__icontains="HDA"), transform=lambda x: x)
