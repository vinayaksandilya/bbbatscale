import pytest
from core.models import Meeting
from django.conf import settings
from django.contrib.auth import get_user_model
from django.test.client import RequestFactory
from oidc_authentication.auth import TenantOIDCAuthenticationBackend, provider_logout
from oidc_authentication.models import AuthParameter

User = get_user_model()


@pytest.fixture(scope="function")
def example_user(db, testserver_tenant) -> User:
    return User.objects.create(username="testikus", tenant=testserver_tenant)


@pytest.fixture(scope="function")
def example_user_meeting(db) -> Meeting:
    return Meeting.objects.create(creator="testikus", room_name="Test room", meeting_id="TestMeetingId")


@pytest.fixture(scope="function")
def example_auth_parameter(db, testserver_tenant) -> AuthParameter:
    return AuthParameter.objects.create(
        tenant=testserver_tenant,
        client_id="Example",
        client_secret="example_secret",
        authorization_endpoint="https://scale.example.org/auth",
        token_endpoint="https://scale.example.org/token",
        user_endpoint="https://scale.example.org/user",
        jwks_endpoint="https://scale.example.org/jwks",
        end_session_endpoint="https://scale.example.org/end",
    )


@pytest.fixture(scope="function")
def example_auth_parameter_custom_username(db, testserver2_tenant) -> AuthParameter:
    return AuthParameter.objects.create(
        tenant=testserver2_tenant,
        client_id="Example2",
        client_secret="example_secret2",
        authorization_endpoint="https://scale.example.org/auth",
        token_endpoint="https://scale.example.org/token",
        user_endpoint="https://scale.example.org/user",
        jwks_endpoint="https://scale.example.org/jwks",
        end_session_endpoint="https://scale.example.org/end",
        oidc_username_standard=False,
    )


def test_update_user(example_user):
    user: User = example_user
    claims = {
        "preferred_username": "testikus",
        "given_name": "Hans",
        "family_name": "Testikus",
        "email": "hans.testikus@example.org",
        "groups": ["superuser", "staff", "supporter", "moderator"],
    }
    TenantOIDCAuthenticationBackend().update_user(user, claims)

    assert user.first_name == "Hans"
    assert user.last_name == "Testikus"
    assert user.email == "hans.testikus@example.org"
    assert user.is_superuser is True
    assert user.is_staff is True
    assert settings.MODERATORS_GROUP in user.groups.values_list("name", flat=True)
    assert settings.SUPPORTERS_GROUP in user.groups.all().values_list("name", flat=True)

    claims_no_support = {
        "preferred_username": "testikus",
        "given_name": "Hans",
        "family_name": "Testikus",
        "email": "hans.testikus@example.org",
        "groups": ["superuser", "moderator"],
    }

    TenantOIDCAuthenticationBackend().update_user(user, claims_no_support)

    assert user.is_superuser is True
    assert user.is_staff is False
    assert settings.MODERATORS_GROUP in user.groups.values_list("name", flat=True)
    assert settings.SUPPORTERS_GROUP not in user.groups.all().values_list("name", flat=True)


def test_provider_logout(example_auth_parameter, example_user):
    get = RequestFactory().get("/")
    assert (
        provider_logout(get)
        == f"{example_auth_parameter.end_session_endpoint}?redirect_uri={get.build_absolute_uri('/')}"
    )


def test_verify_claims(example_auth_parameter, example_auth_parameter_custom_username):
    claims = {
        "given_name": "Hans",
        "family_name": "Testikus",
        "email": "hans.testikus@example.org",
        "groups": ["superuser", "staff", "supporter", "moderator"],
    }
    assert TenantOIDCAuthenticationBackend().verify_claims(claims, necessary_claim=None) is True
    assert TenantOIDCAuthenticationBackend().verify_claims(claims, necessary_claim="preferred_username") is False
    claims = {
        "preferred_username": "Testikus",
        "given_name": "Hans",
        "family_name": "Testikus",
        "email": "hans.testikus@example.org",
        "groups": ["superuser", "staff", "supporter", "moderator"],
    }
    assert TenantOIDCAuthenticationBackend().verify_claims(claims, necessary_claim="preferred_username") is True


def test_get_or_create_user_oidc_username(
    mocker, example_user, testserver_tenant, example_auth_parameter, example_user_meeting
):
    payload = {
        "iss": "https://keycloak.example.org",
        "sub": "8e33862d-fc74-4ec3-a998-7de7218e1550",
        "name": "Test Testikus",
        "preferred_username": "testikus",
        "given_name": "Test Hannelore",
        "family_name": "Testikus",
        "email": "testikus@example.org",
    }
    user_info = {
        "sub": "8e33862d-fc74-4ec3-a998-7de7218e1550",
        "name": "Test Testikus",
        "groups": ["moderator", "superuser", "staff", "supporter"],
        "preferred_username": "testikus",
        "given_name": "Test Hannelore",
        "family_name": "Testikus",
        "email": "testikus@example.org",
    }
    mocker.patch("oidc_authentication.auth.TenantOIDCAuthenticationBackend.get_userinfo", return_value=user_info)
    user = TenantOIDCAuthenticationBackend().get_or_create_user(
        None, None, payload, testserver_tenant, example_auth_parameter
    )

    assert user.username == "8e33862d-fc74-4ec3-a998-7de7218e1550@https://keycloak.example.org"
    assert (
        Meeting.objects.filter(creator="8e33862d-fc74-4ec3-a998-7de7218e1550@https://keycloak.example.org").count() == 1
    )


def test_get_or_create_user_custom_username(
    mocker, example_user, testserver2_tenant, example_auth_parameter_custom_username, example_user_meeting
):
    payload = {
        "iss": "https://keycloak.example.org",
        "sub": "8e33862d-fc74-4ec3-a998-7de7218e1550",
        "name": "Test Testikus",
        "preferred_username": "testikus",
        "given_name": "Test Hannelore",
        "family_name": "Testikus",
        "email": "testikus@example.org",
    }
    user_info = {
        "sub": "8e33862d-fc74-4ec3-a998-7de7218e1550",
        "name": "Test Testikus",
        "groups": ["moderator", "superuser", "staff", "supporter"],
        "preferred_username": "testikus",
        "given_name": "Test Hannelore",
        "family_name": "Testikus",
        "email": "testikus@example.org",
    }
    mocker.patch("oidc_authentication.auth.TenantOIDCAuthenticationBackend.get_userinfo", return_value=user_info)
    user = TenantOIDCAuthenticationBackend().get_or_create_user(
        None, None, payload, testserver2_tenant, example_auth_parameter_custom_username
    )

    assert user.username == "testikus@testserver2"
    assert Meeting.objects.filter(creator="testikus@testserver2").count() == 1
