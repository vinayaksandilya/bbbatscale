# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-09-16 17:28+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: support_chat/templates/support_chat/prepared_answers_overview.html:10
msgid "Prepared Answers"
msgstr ""

#: support_chat/templates/support_chat/prepared_answers_overview.html:14
msgid "Create"
msgstr ""

#: support_chat/templates/support_chat/prepared_answers_overview.html:39
msgid "Create Prepared Answer"
msgstr ""

#: support_chat/templates/support_chat/prepared_answers_overview.html:50
#: support_chat/templates/support_chat/prepared_answers_overview.html:92
msgid "Title"
msgstr ""

#: support_chat/templates/support_chat/prepared_answers_overview.html:58
#: support_chat/templates/support_chat/prepared_answers_overview.html:101
msgid "Text"
msgstr ""

#: support_chat/templates/support_chat/prepared_answers_overview.html:67
#: support_chat/templates/support_chat/prepared_answers_overview.html:115
#: support_chat/templates/support_chat/settings.html:55
msgid "Save"
msgstr ""

#: support_chat/templates/support_chat/prepared_answers_overview.html:80
msgid "Edit Prepared Answer"
msgstr ""

#: support_chat/templates/support_chat/prepared_answers_overview.html:112
msgid "Delete"
msgstr ""

#: support_chat/templates/support_chat/settings.html:23
msgid "Settings"
msgstr ""

#: support_chat/templates/support_chat/settings.html:31
msgid "Enable Support Chat"
msgstr ""

#: support_chat/templates/support_chat/settings.html:41
msgid "Disable Support Chat if offline"
msgstr ""

#: support_chat/templates/support_chat/settings.html:47
msgid "Maximum message length"
msgstr ""

# TODO
#: support_chat/templates/support_chat/supporters_overview.html:13
msgid "Active Supporters"
msgstr ""

# TODO
#: support_chat/templates/support_chat/supporters_overview.html:36
msgid "Inactive Supporters"
msgstr ""
